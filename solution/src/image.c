#include "include/image.h"
#include "malloc.h"

extern inline void set_pixel(struct image* img, size_t x, size_t y, struct pixel source_pixel );

extern inline struct pixel get_pixel(struct image img, size_t x, size_t y);

struct image construct_image(uint32_t width, uint32_t height){
    struct pixel* pixel = malloc(width * height * sizeof(struct pixel));
    return (struct image) {
        .width=width,
        .height=height,
        .data=pixel
    };
}

void free_image(const struct image* img){
    if (img->data !=NULL) {
        free(img->data);
    }
}
