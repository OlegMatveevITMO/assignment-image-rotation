#include "include/util.h"
#include <stdio.h>
#include <stdlib.h>



void check_read_msg(enum read_status status){
    switch (status) {
        case READ_OK:
            printf("file was successfully read\n");
            break;
        case READ_INVALID_HEADER:
            printf("invalid file header\n");
            break;
        case READ_HEADER_ERROR:
            printf("error reading file header\n");
            break;
        case READ_UNEXPECTED_EOF:
            printf("file ended unexpectedly\n");
            break;
            case INVALID_BITS:
                printf("INVALID_BITS\n");
            break;
            case READ_FILE_ERROR:
        default:
            printf("error while reading file\n");
            break;
    }
}
void check_write_msg(enum write_status status){
    switch (status) {
        case WRITE_OK:
            printf("data written successfully to file\n");
            break;
        case WRITE_ERROR:
        default:
            printf("error writing data to file\n");
            break;
    }
}

