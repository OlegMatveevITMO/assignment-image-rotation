#include "include/bmp.h"
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
const uint32_t BMP_SIGNATURE = 0x4D42;
const size_t BIT_PER_COLOR = 24;



struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
//http://www.fastgraph.com/help/bmp_header_format.html

static inline struct bmp_header create_bmp_header(uint64_t width, uint64_t height){
    return (struct bmp_header) {
            .bfType = 0x4d42, // bmp file type
            .bfileSize = width*height*24, //total file size
            .bfReserved = 0, //reserved; must be 0
            .bOffBits = 54,  //the size of the indent to pixels
            .biSize = 40,  //the size of the information header, which is 40
            .biWidth = width, //width
            .biHeight = height, //height
            .biPlanes = 1, //the number of color planes for the target device, always equal to 1
            .biBitCount = 24,  //how much does a pixel contain
            .biCompression = 0, // compression type (0=none, 1=RLE-8, 2=RLE-4)
            .biSizeImage = sizeof(struct bmp_header) + width*height*24, // size of image data in bytes
            .biXPelsPerMeter = 0, // horizontal resolution in pixels per meter
            .biYPelsPerMeter = 0, // vertical resolution in pixels per meter
            .biClrUsed = 0, // number of colors in image
            .biClrImportant = 0 // number of important colors
    };

}

static inline uint8_t get_padding(const uint32_t width) {
    const uint8_t padding = (width * sizeof(struct pixel)) % 4;
    return padding ? 4 - padding : 0;
}


static bool read_header( FILE* f, struct bmp_header* header ) {
    fread(header, sizeof(struct bmp_header), 1, f);
    if (header->bfType!=BMP_SIGNATURE||header->biSize!=40||header->biBitCount!=BIT_PER_COLOR) return false;
    return true;
}







enum read_status
from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = { 0 };

    if (!read_header(in, &header)) {
        if (feof(in))
            return READ_UNEXPECTED_EOF;
        return READ_FILE_ERROR;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(sizeof(struct pixel) * img->height * img->width);

    const size_t padding = get_padding(img->width);

    for (size_t y = 0; y < img->height; y++) {
        if (fread(&(img->data[y * img->width]), sizeof(struct pixel), img->width, in) < 1) {
            if (feof(in)){
                free(img->data);
                return READ_UNEXPECTED_EOF;
            }
            free(img->data);
            return READ_FILE_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            free(img->data);
            return INVALID_BITS;
        }
    }

    return READ_OK;
}




enum write_status to_bmp( FILE* out, struct image const* img ){


    const struct bmp_header header = create_bmp_header(img->width, img->height);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out)<1){
        return WRITE_ERROR;
    }
    const size_t padding = get_padding(img->width);

    const char line_padding[4] = { 0 };

    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; ++i) {
            if (fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out) < 1) {
                return WRITE_ERROR;
            }
            if (fwrite(line_padding, padding, 1, out) < 1) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

enum open_file_status open_bmp_from_file( const char* filename, struct image* image){
    FILE* const in = fopen(filename, "rb");
    if(!in) {
        return OPEN_FAIL;
    }

    const enum read_status status = from_bmp(in, image);
    check_read_msg(status);
    fclose(in);
    if(status != READ_OK){
        return OPENED_BUT_READ_FAIL;
    } else {
        return OPEN_OK;
    }
}

enum save_file_status save_as_bmp( const char* filename, struct image* image){
    FILE* out = fopen(filename, "wb");
    if(!out){
        return SAVE_FAIL;
    }

    const enum write_status status = to_bmp(out, image);
    check_write_msg(status);
    fclose(out);
    if(status != WRITE_OK){
        return SAVE_FAIL;
    }
    else {
        return SAVE_OK;
    }

}

