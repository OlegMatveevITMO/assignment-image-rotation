#include "include/bmp.h"
#include "include/image.h"
#include "include/transformation.h"
#include <stdio.h>



int main( int argc, char** argv ) {
    if (argc != 2 && argc != 3) {
        printf("Use %s file.bmp new_file.bmp\\n",argv[0]);
        return 1;
    }
    struct image img = {0};
    if(open_bmp_from_file(argv[1], &img)) {
        return 1;
    }
    struct image new_img = rotate(img);
    free_image(&img);
    if(save_as_bmp(argv[2], &new_img)) {
        free_image(&new_img);
        return 1;
    }
    free_image(&new_img);
    return 0;
}
