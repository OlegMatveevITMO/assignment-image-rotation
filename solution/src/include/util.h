#ifndef _UTIL_H_
#define _UTIL_H_


enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_FILE_ERROR,
    READ_HEADER_ERROR,
    READ_UNEXPECTED_EOF,
    INVALID_BITS
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum open_file_status {
    OPEN_OK =0,
    OPEN_FAIL,
    OPENED_BUT_READ_FAIL,
};

enum save_file_status {
    SAVE_OK =0,
    SAVE_FAIL,
};

void check_read_msg(enum read_status status);
void check_write_msg(enum write_status status);


#endif
