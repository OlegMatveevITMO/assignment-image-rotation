#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct __attribute__((__packed__)) pixel {
    unsigned char b, g, r;
};

struct image{
    uint64_t width, height;
    struct pixel* data;
};
struct image construct_image(uint32_t width, uint32_t height);

void free_image(const struct image* img);

inline void set_pixel(struct image* img, size_t x, size_t y, struct pixel source_pixel ){
    img->data[ y * img->width + x] = source_pixel;
}

inline struct pixel get_pixel(struct image img, size_t x, size_t y){
    return img.data[ y * img.width + x];
}


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
