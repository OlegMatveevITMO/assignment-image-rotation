#include "include/image.h"
#include "include/transformation.h"



struct image rotate(struct image const source)
{
    struct image new_rotate_image = construct_image(source.height,source.width);

    for (size_t x = 0; x < source.height; x++) {
        for (size_t y = 0; y < source.width; y++) {
            set_pixel(&new_rotate_image,source.height - 1 - x, y ,get_pixel(source,y,x));
        }
    }
    return new_rotate_image;
}
